const { pool } = require("../database/config");

const addTuto = async(req, res) =>{
    try{
        pool.query('INSERT INTO tutorias set ?', [req.body]);
        res.json({
            ok: true,
            text: 'Insertado correctamente',
            data: req.body
        });
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            ok: false,
            Text: 'Algo salió mal',
        });
    }
}

module.exports = {
    addTuto
}