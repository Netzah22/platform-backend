const { pool } = require("../database/config");
const bcrypt = require("bcryptjs");

const registro = async (req, res) => {
    try {
        pool.query('SELECT * FROM alumnos WHERE matricula = ?', [req.body.matricula], async (err, rows) => {
            if (rows.length > 0) {
                res.json({ text: "Usuario ya registrado" });
            } else {
                const matricula = req.body.matricula;
                const salt = bcrypt.genSaltSync();
                req.body.contra = bcrypt.hashSync(req.body.contra, salt);
                pool.query("INSERT INTO alumnos set ?", [req.body]);
                res.json({
                    ok: true,
                    text: 'Registro correcto',
                    matricula: matricula,
                    data: req.body
                });
            }
    });
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            ok: false,
            Text: 'Algo salió mal',
        });
    }
}

const alumnoTutoria = async (req, res) => {
    try {
        pool.query('SELECT * FROM alumno_tutoria WHERE matricula = ?', [req.body.matricula], async (err, rows) => {
            if (rows.length > 0) {
                res.json({ text: "Este usuario ya esta registrado en una tutoría" });
            } else {
                pool.query("INSERT INTO alumno_tutoria set ?", [req.body]);
                res.json({
                    ok: true,
                    text: 'Registro correcto',
                    data: req.body
                });
            }
    });
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            ok: false,
            Text: 'Algo salió mal',
        });
    }
}

module.exports = {
    registro,
    alumnoTutoria
}