const { pool } = require("../database/config");
const bcrypt = require("bcryptjs");

const actualizar = async (req, res) =>{
    try{
        pool.query('SELECT * FROM alumnos WHERE matricula = "admin"', async (err, rows) =>{
            if(rows.length > 0){
                const salt = bcrypt.genSaltSync();
                req.body.contra = bcrypt.hashSync(req.body.contra, salt);
                pool.query('UPDATE alumnos set ? WHERE matricula = "admin"', [req.body, req.body.matricula]);
                res.json({
                    ok: true,
                    text: 'Actualización correcta',
                    data: req.body
                });
            }
        });
    }catch (error){
        console.log(error);
        return res.status(500).json({
            ok: false,
            Text: 'Algo salió mal',
        });
    }
}

module.exports = {
    actualizar
}