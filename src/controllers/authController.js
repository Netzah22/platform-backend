const { pool } = require("../database/config");
const bcrypt = require("bcryptjs");

const login = async (req, res) =>{
    try{
        const matricula = req.body.matricula;
        pool.query('SELECT * FROM alumnos WHERE matricula = ?', [req.body.matricula],async (err, rows) => {
            if (rows.length > 0) {
                bcrypt.compare(
                req.body.contra,
                rows[0].contra,
                function (err, ress){
                    if (err) {
                        res.json({ ok: false, text: '' +  err });
                    }
                    if (ress) {
                            res.json({
                            ok: true,
                            text: 'Login correcto',
                            matricula: matricula,
                            data: rows[0]
                        });
                    } else {
                        res.json({
                        ok: false,
                        text: 'Matricula o contraseña incorrecta'
                        });
                    }
                });
            } else {
                res.json({ 
                    ok: false, 
                    text: 'Matricula o contraseña incorrecta'
                });
            }
        });
    }catch (error) {
        console.log(error);
        return res.status(500).json({
            ok: false,
            text: 'Algo salio mal'
        });
    }
}

module.exports = {
    login
}