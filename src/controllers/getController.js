const { pool } = require('../database/config');

//Obtener todas las tutorias
const getAll = async (req, res) =>{
    return new Promise( (resolve, reject) =>{
        pool.query('SELECT * FROM tutorias', (err, rows)=> {
            if (err) reject(err);
            resolve(rows);
            console.log(rows);
            res.json({
                ok: true,
                text: 'Todas las tutorías',
                data: rows
            });
        });
    });
}

//Obtener una tutoria
const getTuto = async (req,res) => {
    const { id } = req.params;
    return new Promise((resolve, reject) => {
        pool.query('SELECT * FROM tutorias WHERE id = ?',[id],
        (err, rows) => {
            if (err) reject(err.message);
                if(rows.length>0){
                res.json({
                    ok: true,
                    text: 'Tutoria',
                    data: rows[0]
                });
            }else{
                res.json({text:'Tutoria no encontrada'});
                }
        });
    });
}

//Eliminar una tutoria
const eliminar = async (req, res) => {
    const { id } =  req.params;
    return new Promise((resolve, reject) => {
        pool.query('DELETE FROM tutorias WHERE id = ?',[id],
            (err, rows) => {
                if (err) reject(err.message);
                res.json({
                    ok: true,
                    text:'Usuario eliminado'
                });
        });
    });
}


//Obtener una tutoria por nombre
const getTutoAlumno = async (req,res) => {
    const { nombre } = req.params;
    return new Promise((resolve, reject) => {
        pool.query('SELECT * FROM alumno_tutoria WHERE nombre_tutoria = ?',[nombre],
        (err, rows) => {
            if (err) reject(err.message);
                if(rows.length>0){
                res.json({
                    ok: true,
                    text: 'Tutoria',
                    data: rows
                });
            }else{
                res.json({text:'Tutoria no encontrada'});
                }
        });
    });
}

//Obtener usuario actual
const getOne = async (req,res) => {
    const { matricula } = req.params;
    return new Promise((resolve, reject) => {
        pool.query('SELECT * FROM alumnos WHERE matricula = ?',[matricula],
        (err, rows) => {
            if (err) reject(err.message);
                if(rows.length>0){
                res.json({
                    ok: true,
                    text: 'Alumno',
                    data: rows[0]
                });
            }else{
                res.json({text:'Alumno no encontrado'});
                }
        });
    });
}

//Comprobar inscripcion a tutoria
const comprobe = async (req,res) => {
    const { matricula } = req.params;
    return new Promise((resolve, reject) => {
        pool.query('SELECT * FROM alumno_tutoria WHERE matricula = ?',[matricula],
        (err, rows) => {
            if (err) reject(err.message);
                if(rows.length>0){
                res.json({
                    ok: true,
                    text: 'Alumno inscrito',
                    data: rows[0]
                });
            }else{
                res.json({text:'Alumno no encontrado'});
                }
        });
    });
}

//Comprobar inscripcion a tutoria
const getAllTuto = async (req, res) =>{
    return new Promise( (resolve, reject) =>{
        pool.query('SELECT * FROM alumno_tutoria', (err, rows)=> {
            if (err) reject(err);
            resolve(rows);
            console.log(rows);
            res.json({
                ok: true,
                text: 'Todas las inscripciones',
                data: rows
            });
        });
    });
}

module.exports = {
    getAll,
    getOne,
    getTuto,
    comprobe,
    getAllTuto,
    getTutoAlumno,
    eliminar
}