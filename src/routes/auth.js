const { Router } = require('express');
const { login } = require('../controllers/authController');
const { check } = require('express-validator');
const { validar } = require('../middlewares/validar_campos');

const router = Router();

router.post('/auth', [
    check('matricula','La matricula es obligatoria').not().isEmpty(),
    check('contra','La contraseña es obligatoria').not().isEmpty(),
    validar
], login);

module.exports = router;