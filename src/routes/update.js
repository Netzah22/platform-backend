const { Router } = require('express');
const { actualizar } = require('../controllers/updateController');
const { check } = require('express-validator');
const { validar } = require('../middlewares/validar_campos');

const router = Router();

router.post('/update', [
    check('nombre'),
    check('primer_apellido'),
    check('segundo_apellido'),
    check('matricula'),
    check('contra'),
    validar
], actualizar);

module.exports = router;