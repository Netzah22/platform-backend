const { Router } = require('express');
const { getAll, getOne, getTuto, comprobe, getAllTuto, getTutoAlumno, eliminar } = require('../controllers/getController');

const router = Router();

router.get('/routess/list', getAll);
router.get('/routess/getOne/:matricula', getOne);
router.get('/routess/getTuto/:id', getTuto);
router.get('/routess/comprobe/:matricula', comprobe);
router.get('/routess/lista', getAllTuto);
router.get('/routess/getTutoAlumno/:nombre', getTutoAlumno);
router.delete('/routess/eliminar/:id', eliminar);

module.exports = router;