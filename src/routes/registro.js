const { Router } = require('express');
const { registro, alumnoTutoria } = require('../controllers/registroController');
const { check } = require('express-validator');
const { validar } = require('../middlewares/validar_campos');

const router = Router();

router.post('/registro', [
    check('nombre', 'el nombre es obligatorio').not().isEmpty(),
    check('primer_apellido', 'el primer_apellido es obligatorio').not().isEmpty(),
    check('segundo_apellido', 'el segundo_apellido es obligatorio').not().isEmpty(),
    check('carrera', 'la carrera es obligatoria').not().isEmpty(),
    check('grupo', 'el grupo es obligatorio').not().isEmpty(),
    check('matricula', 'la matricula es obligatoria').not().isEmpty(),
    check('numero', 'el numero es obligatorio').not().isEmpty(),
    check('contra', 'la contraseña es obligatoria').not().isEmpty(),
    validar
], registro);

router.post('/alumnoTutoria', [
    check('nombre', 'el nombre es obligatorio').not().isEmpty(),
    check('apellidos', 'los apellidos son obligatorios').not().isEmpty(),
    check('carrera', 'la carrera es obligatoria').not().isEmpty(),
    check('grupo', 'el grupo es obligatorio').not().isEmpty(),
    check('matricula', 'la matricula es obligatoria').not().isEmpty(),
    check('numero', 'el numero es obligatorio').not().isEmpty(),
    check('nombre_tutoria', 'el nombre de la tutoria es obligatorio').not().isEmpty(),
    check('encargado_tutoria', 'el encargado de la tutoria es obligatorio').not().isEmpty(),
    validar
], alumnoTutoria);

module.exports = router;