const { Router } = require('express');
const { addTuto } = require('../controllers/newTuto');
const { check } = require('express-validator');
const { validar } = require('../middlewares/validar_campos');

const router = Router();

router.post('/addTuto', [
    check('nombre', 'el nombre es obligatorio').not().isEmpty(),
    check('encargado', 'el encargado es obligatorio').not().isEmpty(),
    check('descripcion', 'la descripcion es obligatoria').not().isEmpty(),
    check('limitar'),
    check('limite'),
    check('dias', 'seleccione dias'),
    validar
], addTuto);

module.exports = router;